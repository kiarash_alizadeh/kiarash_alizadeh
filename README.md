<meta http-equiv="Permissions-Policy" content="interest-cohort=(), user-id=()" />

<pre>
<h1 align="center">
  <br>
  <img src="logo.PNG" alt="KiarashAlizadeh" width="200">
  <br>
  ＫＩΛＲΛＳＨ ΛＬＩＺΛＤΞＨ
</h1><h4 align="center"> 𝓐 𝓳𝓾𝓷𝓲𝓸𝓻 𝓯𝓻𝓸𝓷𝓽 𝓮𝓷𝓭 𝓭𝓮𝓿𝓮𝓵𝓸𝓹𝓮𝓻</h4>
</pre>
 <br>
 
## 🛠️ My expertise

<p>
  
- front-end：

  [<img alt="HTML5" src="icons/html5.svg" title="HTML5"/>](https://developer.mozilla.org/en-US/docs/Glossary/HTML5)
  [<img alt="CSS3" src="icons/css3.svg" title="CSS3"/>](https://developer.mozilla.org/en-US/docs/Web/CSS)
  [<img alt="JavaScript" src="icons/javascript.svg" title="JavaScript"/>](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
  [<img alt="bootstrap" src="icons/bootstrap.svg" title="bootstrap"/>](https://getbootstrap.com/)
  [<img alt="sass" src="icons/sass.svg" title="sass"/>](https://sass-lang.com/)


- Languages:

  [<img alt="Python" src="icons/Python.svg" title="Python"/>](https://www.python.org/)
  [<img alt="C#" src="icons/c-sharp.svg" title="C#"/>](https://learn.microsoft.com/en-us/dotnet/csharp/)

- Cms:

  [<img alt="WordPress" src="icons/WordPress.svg" title="WordPress" />](https://wordpress.org/)
    
- DataBase:

  [<img alt="mssqlserver" src="icons/Ms-Sql-Server.svg" title="mssqlserver"/>](https://www.microsoft.com/en-us/sql-server/)  

- Version Control：

  [<img alt="Git" src="icons/Git.svg"/>](https://git-scm.com/)
  [<img alt="GitLab" src="icons/GitLab.svg" title="GitLab"/>](https://gitlab.com/)
  [<img alt="GitHub" src="icons/GitHub.svg" title="GitHub"/>](https://github.com/)

- Graphic & Ui Design:

  [<img alt="figma" src="icons/figma.svg" title="figma"/>](https://www.figma.com/)
  [<img alt="photoshop" src="icons/Photoshop.svg" title="photoshop"/>](https://www.adobe.com/products/photoshop.html)
<br>

- 🌱 I’m currently learning
[<img alt="React" src="icons/React.svg" title="React"/>](https://react.dev/)
& [<img alt="TailwindCSS" src="icons/tailwindcss.svg" title="TailwindCSS" />](https://tailwindcss.com/)
</p>

<br>

## 🌐 Languages

| Language | Proficiency     |
| -------- | --------------- |
| English  | fluent          |
| German   | B2              |
| Persian  | Native language |

<br>

## 📫 𝙷𝚘𝚠 𝚝𝚘 𝚛𝚎𝚊𝚌𝚑 𝚖𝚎:

𝚈𝚘𝚞 𝚌𝚊𝚗 𝚛𝚎𝚊𝚌𝚑 𝚖𝚎 𝚊𝚝 𝚝𝚑𝚎 𝚎𝚖𝚊𝚒𝚕 𝚒𝚗 𝚖𝚢 𝚐𝚒𝚝𝚑𝚞𝚋 𝚙𝚛𝚘𝚏𝚒𝚕𝚎 Or in 𝚖𝚢 𝚜𝚘𝚌𝚒𝚊𝚕𝚜!

[<img src="social/telegram.svg" height="40em" align="center" alt="Contact KiarashAlizadeh on Telegram" title="Contact Contact KiarashAlizadeh on Telegram"/>](https://t.me/kiarash_alizadeh) 
[<img src="social/instagram.svg" height="40em" align="center" alt="Follow KiarashAlizadeh on Instagram" title="Follow KiarashAlizadeh on Instagram"/>](https://instagram.com/kiarash_alizadehh)
[<img src="social/twitter-x.png" height="40em" align="center" alt="Follow KiarashAlizadeh on Twitter" title="Follow KiarashAlizadeh on Twitter"/>](https://twitter.com/kiarashAlizadeh)
[<img src="social/linkedin.png" height="40em" align="center" alt="Follow KiarashAlizadeh on LinkedIn" title="Follow KiarashAlizadeh on LinkedIn"/>](https://www.linkedin.com/in/kiarash-alizadeh-13319222b)

